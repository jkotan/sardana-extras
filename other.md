# Other Sardana related projects

Below you will find a list of Sardana related projects e.g. GUIs, widgets, tools, utilities, etc.

| Name | Description | Link(s) to project |
| ---- | ----------- | ------------ |
| LaVue | An online live viewer to show 2D live images from xray-detectors  | [**LaVue**](https://github.com/lavue-org/lavue) |
